# SimpleApp

SimpleApp is a sample project demonstrating the setup of a continuous integration (CI) and continuous deployment (CD) pipeline using GitLab and Jenkins.

## Overview

This project consists of a simple standalone application written in Python. This is an application to find the factorial of a number

The CI/CD pipeline is set up using Jenkins, which integrates with the GitLab repository. The pipeline includes stages for checkout, build, test, and deploy.

## Getting Started

To set up and run the CI/CD pipeline for SimpleApp, follow these steps:

### Prerequisites

- GitLab account
- Jenkins installed and configured


### Sample Application

1. Create a simple standalone application in Python.
2. Example code (`app.py`):

    ```python
    # simple_app.py
    def main():
        print("Hello, world!")

    if __name__ == "__main__":
        main()
    ```

### Jenkins Setup

1. Install Jenkins on your local machine or a server.
2. Configure Jenkins to integrate with your GitLab repository using the GitLab Plugin.

### Create Jenkins Pipeline

1. Create a Jenkins pipeline using a Jenkinsfile.
2. Example Jenkinsfile is provided in the repository.

### GitLab CI Configuration

1. Create a `.gitlab-ci.yml` file in the root of your GitLab repository.
2. Configure GitLab CI to trigger the Jenkins pipeline on each commit to the repository.



### Testing

- Test the CI/CD pipeline by making a commit to the GitLab repository.
- The pipeline has been executed successfully and deploys the application to the test environment.


