def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

def main():
    print("Welcome to Factorial Calculator App!")
    while True:
        try:
            num = int(input("Enter a non-negative integer to calculate its factorial: "))
            if num < 0:
                print("Please enter a non-negative integer.")
                continue
            result = factorial(num)
            print("Factorial of", num, "is:", result)
            break
        except ValueError:
            print("Invalid input. Please enter an integer.")

if _name_ == "_main_":
    main()